# Using a Catalog Resolver

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-10_0_0_j/samples/sample-name`.*

---
*A code sample to show how to instruct XML Compare to make use of catalog files.*

This document describes how to run the sample. For concept details see: [Using a Catalog Resolver](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/using-a-catalog-resolver)

The sample code essentially loads two input files (that require catalog support), compares them, and produces an output that is saved to a file (out.xml). The DTD referenced in the DOCTYPE of the samples files (a.xml and b.xml) does not exist, therefore a catalog (catalog.xml) is required to provide the DTDs. Furthermore, the DTD provides a default attribute (/root/@​i='42') that does not appear in the input data, but does appear in the full-context delta result.  These files are in this repo.

## Running the Sample via the Command-line 

The command-line version of this sample can be run by issuing the following command, replacing x.y.z with the major.minor.patch version number of your release e.g. `command-10.0.0.jar`:

```
java -jar ../../command-x.y.z.jar compare delta a.xml b.xml out.xml
```

## Running the Sample via the API

The API version of the sample is designed to be built and run via the Ant build technology. The provided build.xml script has two main targets

*  run - the default target which compiles and runs the sample code.
*  clean - returns the sample to its original state.

It can be run by issuing either the `ant` or the `ant run` commands.

Alternatively, the sample can be manually compiled and run using the following Java commands, assuming that both the Java compiler and runtime platforms are available on the command-line, replacing x.y.z with the major.minor.patch version number of your release e.g. `deltaxml-10.0.0.jar`:  




```
javac -cp ../../deltaxml-x.y.z.jar:../../saxon9pe.jar:../../resolver.jar FilesWithCatalog.java
java -cp .:../../deltaxml-x.y.z.jar:../../saxon9pe.jar:../../resolver.jar:../../xercesImpl.jar FilesWithCatalog  
```

Note that you need to ensure that you use the correct directory and class path separators for your operating system.