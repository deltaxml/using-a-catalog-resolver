// Copyright (c) 2024 Deltaman group limited. All rights reserved.

// $Id$

import java.io.File;
import java.io.IOException;

import net.sf.saxon.s9api.Serializer;

import com.deltaxml.core.ComparatorInstantiationException;
import com.deltaxml.core.FeatureNotRecognizedException;
import com.deltaxml.core.ParserInstantiationException;
import com.deltaxml.cores9api.ComparisonCancelledException;
import com.deltaxml.cores9api.ComparisonException;
import com.deltaxml.cores9api.FilterProcessingException;
import com.deltaxml.cores9api.LicenseException;
import com.deltaxml.cores9api.PipelineLoadingException;
import com.deltaxml.cores9api.PipelineSerializationException;
import com.deltaxml.cores9api.PipelinedComparatorS9;

/**
 * <p>
 * Demonstrates the use of the in-built Catalog resolver.
 * </p>
 * <p>
 * The DTD referenced in the DOCTYPE of the samples files (a.xml and b.xml) does not exist, therefore the catalog is required to
 * provide the DTDs. Furthermore, the DTD provides a default attribute (/root/@i='42') that does not appear in the input data, but
 * does appear in the full-context delta result.
 * </p>
 * <p>
 * In order for this example to work, the catalog resolver needs to be configured so that it can locate its catalog file(s). This
 * can be done in Code, via a configuration properties file, or via system properties. We've used a DeltaXML configuration
 * properties file in this exmample to set the resolver value (see deltaxmlConfig.xml in this directory).
 * </p>
 *
 * @see http://www.xml.com/lpt/a/1378
 * @see http://xml.apache.org/commons/components/resolver/resolver-article.html
 * @see http://xerces.apache.org/xerces2-j/faq-xcatalogs.html
 */
public class FilesWithCatalog
{
  public static void main(String[] args) throws ParserInstantiationException, ComparatorInstantiationException,
      FeatureNotRecognizedException, IllegalArgumentException, ComparisonException, FilterProcessingException,
      PipelineLoadingException, PipelineSerializationException, LicenseException, IOException, ComparisonCancelledException
  {

    // A way to directly set the catalog search path to be used, instead of that provided by an external configuration file, is
    // to uncomment the following line. Note, set this property before the DeltaXML API used, otherwise its value may not be read.
    // System.setProperty("xml.catalog.files", "catalog.xml");

    PipelinedComparatorS9 pc= new PipelinedComparatorS9();
    pc.setComparatorFeature("http://deltaxml.com/api/feature/isFullDelta", true);
    pc.setOutputProperty(Serializer.Property.INDENT, "yes");
    pc.compare(new File("a.xml"), new File("b.xml"), new File("out.xml"));
  }
}
